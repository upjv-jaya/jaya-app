import Environment from './environment'

export const JAYA_LOCAL_KEY = 'jaya_app_miage'

export const DATA_API_URL = Environment.development ? 'http://localhost:4000' : 'https://or8sajuel0.execute-api.eu-west-2.amazonaws.com/v1'
export const USER_API_URL = Environment.development ? 'http://localhost:4001' : 'https://axpdw0oaz4.execute-api.eu-west-2.amazonaws.com/v1'

const BASE_HEADERS = {
  'Content-Type': 'application/json'
}

export function getHttpHeaders () {
  return { ...BASE_HEADERS }
}

export function getAuthenticatedHttpHeaders () {
  let token = window.localStorage.getItem(JAYA_LOCAL_KEY)
  if (token != null) {
    return {
      ...BASE_HEADERS,
      'Authorization': `Bearer ${token}`
    }
  } else {
    throw new Error('TOKEN DOES NOT EXISTS')
  }
}
