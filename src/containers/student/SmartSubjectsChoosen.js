import * as React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Loading } from '../../components'
import { makeQuery } from '../../services/data-service'

const withUserId = connect(
  state => ({
    userId: state.auth.id,
    schoolYearId: state.auth.info.specialization.id
  }),
  null
)

class SmartSubjectsChoosen extends React.Component {
  static propTypes = {
    userId: PropTypes.string.isRequired,
    schoolYearId: PropTypes.string.isRequired
  }

  state = {
    choicesBySkill: {
      firstSemester: null,
      lastSemester: null
    },
    firstSemesterSubjects: null,
    lastSemesterSubjects: null,
    loading: true
  }

  componentDidMount () {
    this._getData()
  }

  render () {
    const { loading, choicesBySkill, firstSemesterSubjects, lastSemesterSubjects } = this.state

    return (
      <div className='subjectChoosenContainer' style={{ marginBottom: '2rem' }}>
        <h3 className='title is-3'>Concernant les choix...</h3>
        {loading ? <Loading /> : (
          <section>
            <div>
              <h4 className='title is-4'>Premier semestre</h4>
              <h5 className='title is-5' style={{ marginBottom: '0rem' }}>Mes choix</h5>
              {choicesBySkill.firstSemester.length > 0 && choicesBySkill.firstSemester.map((s) => (
                <div>
                  <h5 className='title is-6' style={{ marginTop: '1rem', marginBottom: '0.2rem' }}>{s.label}</h5>
                  {s.choices.map(c => (
                    <p>Position: {c.rank}, {c.label}</p>
                  ))}
                </div>
              ))}
              {choicesBySkill.firstSemester.length === 0 && (<p>Pas encore de choix effectués pour cette période.</p>)}
              <h5 className='title is-5' style={{ marginBottom: '0rem', marginTop: '2rem' }}>Choix validés</h5>
              {firstSemesterSubjects != null && firstSemesterSubjects.map((s) => (
                <div>
                  <h5 className='title is-6' style={{ marginTop: '1rem', marginBottom: '0.2rem' }}>{s.label}</h5>
                  {s.choices.map(c => (
                    <p>{c.label}</p>
                  ))}
                </div>
              ))}
              {firstSemesterSubjects == null && (<p>Pas encore de choix validés pour cette période</p>)}
            </div>
            <div style={{ marginTop: '2rem' }}>
              <h4 className='title is-4'>Second semestre</h4>
              {choicesBySkill.lastSemester.length > 0 && choicesBySkill.lastSemester.map((s) => (
                <div>
                  <h5 className='title is-6' style={{ marginTop: '1rem', marginBottom: '0.2rem' }}>{s.label}</h5>
                  {s.choices.map(c => (
                    <p>Position: {c.rank}, {c.label}</p>
                  ))}
                </div>
              ))}
              {choicesBySkill.lastSemester.length === 0 && (<p>Pas encore de choix effectués pour cette période.</p>)}
              <h5 className='title is-5' style={{ marginBottom: '0rem', marginTop: '2rem' }}>Choix validés</h5>
              {lastSemesterSubjects != null && lastSemesterSubjects.map((s) => (
                <div>
                  <h5 className='title is-6' style={{ marginTop: '1rem', marginBottom: '0.2rem' }}>{s.label}</h5>
                  {s.choices.map(c => (
                    <p>{c.label}</p>
                  ))}
                </div>
              ))}
              {lastSemesterSubjects == null && (<p>Pas encore de choix validés pour cette période</p>)}
            </div>
          </section>
        )}
      </div>
    )
  }

  _getData = () => {
    const {
      userId,
      schoolYearId
    } = this.props

    const userChoicesPromise = makeQuery({
      query: `
        query {
          subjectChoicesByUser(studentId: ${userId}) {
            rank
            specializationSubject {
              skill { label }
              subject { label semester }
            }
          }
        }
      `
    })

    const activeSubjectChoicesVersions = makeQuery({
      query: `
        query {
          activesSubjectChoicesVersionsBySchoolYearForStudent(
            schoolYearId: ${schoolYearId}
            studentId: ${userId}
          ) {
            isActive
            semester
            choices {
              specializationSubject {
                skill { label }
                subject { label }
              }
            }
          }
        }
      `
    })

    Promise.all([userChoicesPromise, activeSubjectChoicesVersions])
      .then(this._setData)
      .catch((e) => {
        console.error(e)
        window.alert('Impossible de récupérer les données !')
      })
  }

  _setData = ([ userChoices, activeSubjectChoices ]) => {
    const sortUserChoicesBySkills = (acc, item) => {
      const skillIndex = acc.findIndex(skill => skill.label === item.specializationSubject.skill.label)

      if (skillIndex === -1) {
        return [
          ...acc,
          {
            label: item.specializationSubject.skill.label,
            choices: [{
              rank: item.rank,
              label: item.specializationSubject.subject.label
            }]
          }
        ]
      } else {
        acc[skillIndex].choices = [
          ...acc[skillIndex].choices,
          {
            rank: item.rank,
            label: item.specializationSubject.subject.label
          }
        ]
      }

      return acc
    }

    const sortActiveChoicesBySkills = (acc, item) => {
      const skillIndex = acc.findIndex(skill => skill.label === item.specializationSubject.skill.label)

      if (skillIndex === -1) {
        return [
          ...acc,
          {
            label: item.specializationSubject.skill.label,
            choices: [{
              label: item.specializationSubject.subject.label
            }]
          }
        ]
      } else {
        acc[skillIndex].choices = [
          ...acc[skillIndex].choices,
          {
            label: item.specializationSubject.subject.label
          }
        ]
      }

      return acc
    }

    const firstSemester = userChoices
      .filter(c => c.specializationSubject.subject.semester === 'SEMESTER_1')
      .reduce(sortUserChoicesBySkills, [])

    const lastSemester = userChoices
      .filter(c => c.specializationSubject.subject.semester === 'SEMESTER_2')
      .reduce(sortUserChoicesBySkills, [])

    let firstSemesterSubjects = activeSubjectChoices.find(ac => ac.semester === 'SEMESTER_1')
    if (firstSemesterSubjects != null) {
      firstSemesterSubjects = firstSemesterSubjects.choices.reduce(sortActiveChoicesBySkills, [])
    }

    let lastSemesterSubjects = activeSubjectChoices.find(ac => ac.semester === 'SEMESTER_2')
    if (lastSemesterSubjects != null) {
      lastSemesterSubjects = lastSemesterSubjects.choices.reduce(sortActiveChoicesBySkills, [])
    }

    this.setState({
      choicesBySkill: { firstSemester, lastSemester },
      firstSemesterSubjects,
      lastSemesterSubjects,
      loading: false
    })
  }
}

export default withUserId(SmartSubjectsChoosen)
