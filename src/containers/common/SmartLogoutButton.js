import React from 'react'
import { connect } from 'react-redux'

import { logout } from '../../actions/auth'
import history from '../../routing/history'

const SmartLogoutButton = connect(
  null,
  (dispatch) => ({
    onClick: () => {
      history.push('/')
      dispatch(logout())
    }
  })
)(({ onClick }) => (
  <button
    onClick={onClick}
    className='button is-primary'
  >
    Deconnexion
  </button>
))

export default SmartLogoutButton
