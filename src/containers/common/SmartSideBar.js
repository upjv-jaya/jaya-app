import { connect } from 'react-redux'
import { SideBar } from '../../components/index'
import { logout } from '../../actions/auth'
import history from '../../routing/history'

export default connect(
  state => ({
    userType: state.auth.userType
  }),
  dispatch => ({
    onDisconnectClick: () => {
      dispatch(logout())
      history.push('/')
    }
  })
)(SideBar)
