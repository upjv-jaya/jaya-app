import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faPlusCircle from '@fortawesome/fontawesome-free-solid/faPlusCircle'

import { Loading } from '../../components'

import { makeQuery } from '../../services/data-service'
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit'
import faTrashAlt from '@fortawesome/fontawesome-free-solid/faTrashAlt'

class SmartTeachingSummary extends React.Component {
  state = {
    loading: true,
    subjects: null
  }

  componentDidMount () {
    this._getSubjects()
  }

  render () {
    const { loading, subjects } = this.state

    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem', paddingBottom: '3rem' }}>
        <h3 className='title is-3'>Vos cours enregistrés</h3>
        <section style={{ marginBottom: '2rem' }}>
          <h3 className='title is-4'>Options :</h3>
          <Link className='button is-primary' to='/professor/subject/add'>
            <FontAwesomeIcon icon={faPlusCircle} style={{ marginRight: '0.5rem' }} />
            Ajouter une UE
          </Link>
        </section>
        {(loading) && <Loading />}
        {(!loading && subjects) && (
          <div style={{ marginTop: '2rem', paddingLeft: '1rem' }}>
            <table className='table'>
              <thead>
                <tr>
                  <th>Nom de la matière</th>
                  <th>Heures de TD par groupes</th>
                  <th>Heure de cours</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {subjects.map(({ id, subject, tutorialHoursPerGroup, lectureHours }) => (
                  <tr key={id}>
                    <td>{subject.label}</td>
                    <td>{tutorialHoursPerGroup}</td>
                    <td>{lectureHours}</td>
                    <td>
                      <Link
                        to={`/professor/subject/modify/${id}`}
                        className='button is-primary'
                        style={{ marginRight: '0.5rem' }}>
                        <FontAwesomeIcon icon={faEdit} style={{ marginRight: '0.5rem', marginBottom: '0.2rem' }} /> Modifier
                      </Link>
                      <button
                        className='button is-primary'
                        onClick={() => this._deleteSubject(id)}
                      >
                        <FontAwesomeIcon icon={faTrashAlt} style={{ marginRight: '0.5rem' }} /> Supprimer
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    )
  }

  _getSubjects = () => {
    const { professorId } = this.props

    const query = `
      query {
        professorSubjectsByProfessorId (id: "${professorId}") {
          id
          subject { label }
          tutorialHoursPerGroup
          lectureHours
        }
      }
    `

    this.setState({ loading: true })

    makeQuery({ query })
      .then(subjects => this.setState({ loading: false, subjects }))
      .catch(e => {
        console.error(e)
        window.alert('Impossible de récupérer vos UE')
      })
  }

  _deleteSubject = id => {
    const query = `
      mutation {
        deleteProfessorSubject (${id}: ID) { dataId }
      }
    `

    makeQuery({ query })
      .then(() => this._getSubjects())
      .catch(() => window.alert('Impossible de supprimer une matière, rechargez la page ou contactez le support'))
  }
}

export default connect(
  state => ({
    professorId: state.auth.id
  })
)(SmartTeachingSummary)
