import React from 'react'
import PropTypes from 'prop-types'

import { Loading } from '../../components'
import { makeQuery } from '../../services/data-service'
import history from '../../routing/history'
import SmartTeachingForm from './SmartTeachingForm'
import { connect } from 'react-redux'

class SmartTeachingFormModify extends React.Component {
  static propTypes = {
    professorId: PropTypes.string,
    match: PropTypes.object
  }

  state = {
    loading: true,
    paramError: false,
    subject: null
  }

  componentDidMount () {
    this._getSubject()
  }

  render () {
    const { loading, subject } = this.state

    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem', paddingBottom: '3rem' }}>
        <h3 className='title is-3'>Modification d'une matière</h3>
        <section>
          {loading ? <Loading /> : (
            <SmartTeachingForm
              professorId={this.props.professorId}
              subject={subject}
              onSuccess={this._returnToHome}
              onCancel={this._returnToHome}
            />
          )}
        </section>
      </div>
    )
  }

  _returnToHome = () => history.push('/professor/home')

  _getSubject = () => {
    const { id } = this.props.match.params

    const query = `
      query {
        professorSubjectById (id: "${id}") {
          id
          subject { id label }
          tutorialHoursPerGroup
          lectureHours
        }
      }
    `

    makeQuery({ query, withAuthorization: true })
      .then(subject => {
        this.setState({ subject, loading: false })
      })
      .catch(e => {
        console.error(e)
        window.alert('Impossible de récupérer la matière.')
      })
  }
}

export default connect(
  state => ({
    professorId: state.auth.id
  })
)(SmartTeachingFormModify)
