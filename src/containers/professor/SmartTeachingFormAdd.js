import React from 'react'
import PropTypes from 'prop-types'

import history from '../../routing/history'
import SmartTeachingForm from './SmartTeachingForm'
import { connect } from 'react-redux'

class SmartTeachingFormAdd extends React.Component {
  static propTypes = {
    professorId: PropTypes.string
  }

  render () {
    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem', paddingBottom: '3rem' }}>
        <h3 className='title is-3'>Ajout d'une matière</h3>
        <section>
          <SmartTeachingForm
            professorId={this.props.professorId}
            onSuccess={this._returnToHome}
            onCancel={this._returnToHome}
          />
        </section>
      </div>
    )
  }

  _returnToHome = () => history.push('/professor/home')
}

export default connect(
  state => ({
    professorId: state.auth.id
  })
)(SmartTeachingFormAdd)
