import React from 'react'
import PropTypes from 'prop-types'

import { TeachingForm } from '../../components'
import { makeQuery } from '../../services/data-service'

class SmartTeachingForm extends React.Component {
  static propTypes = {
    professorId: PropTypes.string,
    subject: PropTypes.shape({
      id: PropTypes.string,
      tutorialHoursPerGroup: PropTypes.number,
      lectureHours: PropTypes.number,
      subject: PropTypes.shape({
        id: PropTypes.string,
        label: PropTypes.string
      })
    }),
    onSuccess: PropTypes.func,
    onCancel: PropTypes.func
  }

  state = {
    subject: {
      value: null,
      error: null
    },
    lecture: {
      value: 0,
      error: null
    },
    tutorial: {
      value: 0,
      error: null
    },
    error: null,
    loading: false
  }

  componentDidMount () {
    const { subject } = this.props
    if (subject != null) {
      this._setSubject()
    }
  }

  render () {
    const {
      subject,
      lecture,
      tutorial,
      error,
      loading
    } = this.state

    return (
      <TeachingForm
        modify={this.props.subject != null}
        subject={subject}
        lecture={lecture}
        tutorial={tutorial}
        error={error}
        loading={loading}
        onChange={this._onChange}
        onFormSubmit={this._onSubmit}
        onCancel={this.props.onCancel}
      />
    )
  }

  _onChange = evt => {
    const { name, value } = evt.target

    this.setState({
      [name]: {
        value,
        error: null
      }
    })
  }

  _onSubmit = evt => {
    evt.preventDefault()
    const { professorId, onSuccess } = this.props
    const {
      subject,
      lecture,
      tutorial
    } = this.state

    this.setState({ loading: true })

    if (this.props.subject != null) {
      const query = `
        mutation {
          updateProfessorSubject(
            id: "${this.props.subject.id}"
            professorId: "${professorId}"
            subjectId: "${subject.value.id}"
            lectureHours: ${lecture.value}
            tutorialHours: ${tutorial.value}
          ) {
            id
          }
        }
      `

      makeQuery({ query, withAuthorization: true })
        .then(() => onSuccess())
        .catch(() => window.alert('Impossible de mettre à jour la matière'))
    } else {
      const query = `
        mutation {
          addProfessorSubject(
            professorId: "${professorId}"
            subjectId: "${subject.value.id}"
            lectureHours: ${lecture.value}
            tutorialHours: ${tutorial.value}
          ) {
            id
          }
        }
      `

      makeQuery({ query, withAuthorization: true })
        .then(() => onSuccess())
        .catch(() => window.alert('Impossible de mettre à jour la matière'))
    }
  }

  _setSubject = () => {
    const { subject } = this.props
    this.setState({
      subject: {
        value: {
          id: subject.subject.id,
          label: subject.subject.label
        },
        error: null
      },
      lecture: {
        value: subject.tutorialHoursPerGroup,
        error: null
      },
      tutorial: {
        value: subject.lectureHours,
        error: null
      }
    })
  }
}

export default SmartTeachingForm
