import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { Loading } from '../../components'
import { makeQuery } from '../../services/data-service'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit'
import faTrashAlt from '@fortawesome/fontawesome-free-solid/faTrashAlt'
import faPlusCircle from '@fortawesome/fontawesome-free-solid/faPlusCircle'

class SmartAdminsHome extends React.Component {
  static propTypes = {
    adminId: PropTypes.string
  }

  state = {
    loading: false,
    admins: null
  }

  componentDidMount () {
    this._updateAdmins()
  }

  render () {
    const { loading, admins } = this.state
    const { adminId } = this.props

    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem', paddingBottom: '3rem' }}>
        <h3 className='title is-3'>Gestion des administrateurs JAYA</h3>
        {!loading && (
          <section style={{ marginBottom: '2rem' }}>
            <h3 className='title is-4'>Options :</h3>
            <Link className='button is-primary' to='/admin/admins/add'><FontAwesomeIcon icon={faPlusCircle} style={{ marginRight: '0.5rem' }} />Ajouter un admin</Link>
          </section>
        )}
        <section>
          <h3 className='title is-4'>Liste des administrateurs :</h3>
        </section>
        <section>
          {(loading) && <Loading />}
          {(!loading && admins) && (
            <div style={{ marginTop: '2rem', paddingLeft: '1rem' }}>
              <table className='table'>
                <thead>
                  <tr>
                    <th>Identifiant</th>
                    <th>Email</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {admins.map(({ id, credentials }) => (
                    <tr key={id}>
                      <td>{id}</td>
                      <td>{credentials.email}</td>
                      <td>
                        {adminId !== id && (
                          <React.Fragment>
                            <Link
                              to={`/admin/admins/modify/${id}`}
                              className='button is-primary'
                              style={{ marginRight: '0.5rem' }}>
                              <FontAwesomeIcon icon={faEdit} style={{ marginRight: '0.5rem', marginBottom: '0.2rem' }} /> Modifier
                            </Link>
                            <button
                              className='button is-primary'
                              onClick={() => this._deleteSkill(id)}
                            >
                              <FontAwesomeIcon icon={faTrashAlt} style={{ marginRight: '0.5rem' }} /> Supprimer
                            </button>
                          </React.Fragment>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </section>
      </div>
    )
  }

  _deleteAdmin = id => {
    this.setState({ loading: true })
    const query = `
      mutation {
        deleteAdmin (id: ${id}) {
          dataId
        }
      }
    `

    makeQuery({ query, withAuthorization: true })
      .then(() => this._updateSkills())
      .catch(() => window.alert('Impossible de supprimer un administrateur, rechargez la page ou contactez le support'))
  }

  _updateAdmins = () => {
    this.setState({ loading: true })

    const query = `
      query {
        allAdmins {
          id
          credentials {
            id
            email
          }
        }
      }
    `

    makeQuery({ query, withAuthorization: false })
      .then(admins => this.setState({ loading: false, admins }))
      .catch(() => window.alert('Impossible de récupérer les administrateurs ! Rechargez la page !'))
  }
}

export default connect(
  state => ({
    adminId: state.auth.id
  })
)(SmartAdminsHome)
