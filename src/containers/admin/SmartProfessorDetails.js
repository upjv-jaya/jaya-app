import React from 'react'

import { Loading } from '../../components'
import { makeQuery } from '../../services/data-service'

class SmartSpecializationsDetails extends React.Component {
  state = {
    loading: true,
    subjects: null
  }

  componentDidMount () {
    this._getSubjects()
  }

  render () {
    const { loading, subjects } = this.state

    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem', paddingBottom: '3rem' }}>
        <h3 className='title is-3'>Cours enregistrés</h3>
        {(loading) && <Loading />}
        {(!loading && subjects) && (
          <div style={{ marginTop: '2rem', paddingLeft: '1rem' }}>
            <table className='table'>
              <thead>
                <tr>
                  <th>Nom de la matière</th>
                  <th>Heures de TD par groupes</th>
                  <th>Heure de cours</th>
                </tr>
              </thead>
              <tbody>
                {subjects.map(({ id, subject, tutorialHoursPerGroup, lectureHours }) => (
                  <tr key={id}>
                    <td>{subject.label}</td>
                    <td>{tutorialHoursPerGroup}</td>
                    <td>{lectureHours}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    )
  }

  _getSubjects = () => {
    const { id } = this.props.match.params

    const query = `
      query {
        professorSubjectsByProfessorId (id: "${id}") {
          id
          subject { label }
          tutorialHoursPerGroup
          lectureHours
        }
      }
    `

    this.setState({ loading: true })

    makeQuery({ query, withAuthorization: true })
      .then(subjects => this.setState({ loading: false, subjects }))
      .catch(e => {
        console.error(e)
        window.alert('Impossible de récupérer vos UE')
      })
  }
}

export default SmartSpecializationsDetails
