import React from 'react'

import SmartAdminForm from './SmartAdminForm'
import history from '../../routing/history'

class SmartAdminsAdd extends React.Component {
  render () {
    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem' }}>
        <h3 className='title is-3'>Ajout d'un administrateur</h3>
        <section>
          <SmartAdminForm
            onSuccess={this._returnToList}
            onCancel={this._returnToList}
          />
        </section>
      </div>
    )
  }

  _returnToList = () => history.push('/admin/admins')
}

export default SmartAdminsAdd
