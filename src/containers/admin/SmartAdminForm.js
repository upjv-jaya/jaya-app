import React from 'react'
import PropTypes from 'prop-types'

import { AdminForm } from '../../components'
import { makeQuery } from '../../services/data-service'
import { adminSetPassword } from '../../services/user-service'

class SmartAdminForm extends React.Component {
  static propTypes = {
    admin: PropTypes.shape({
      id: PropTypes.string,
      credentials: PropTypes.shape({
        id: PropTypes.string,
        email: PropTypes.string
      })
    }),
    onSuccess: PropTypes.func,
    onCancel: PropTypes.func
  }

  state = {
    email: {
      value: '',
      error: null
    },
    password: {
      value: '',
      error: null
    },
    loading: false,
    error: null
  }

  componentDidMount () {
    if (this.props.admin != null) {
      this._setAdmin()
    }
  }

  render () {
    const {
      email,
      password,
      loading,
      error
    } = this.state

    const {
      onCancel,
      admin
    } = this.props

    return (
      <AdminForm
        modify={admin != null}
        email={email}
        password={password}
        loading={loading}
        error={error}
        onChange={this._onChange}
        onFormSubmit={this._onSubmit}
        onCancel={onCancel}
      />
    )
  }

  _onChange = evt => {
    const { name, value } = evt.target

    this.setState({
      [name]: {
        value,
        error: null
      }
    })
  }

  _onSubmit = evt => {
    evt.preventDefault()
    const { admin, onSuccess } = this.props

    const {
      email,
      password
    } = this.state

    if (admin) {
      const query = `
        mutation {
          updateAdmin (
            id: "${admin.id}"
            email: "${email.value}"
          ) {
            id
          }
        }
      `
      this.setState({ loading: true })

      makeQuery({ query, withAuthorization: true })
        .then(() => onSuccess())
        .catch(e => {
          console.error(e)
          window.alert('Impossible de modifier l\'utilisateur.')
        })
    } else {
      const query = `
        mutation {
          createAdmin (
            email: "${email.value}"
            password: "NOT_A_PASSWORD"
          ) {
            credentials { id }
          }
        }
      `

      this.setState({ loading: true })

      makeQuery({ query })
        .then(admin => {
          return adminSetPassword(admin.credentials.id, password.value)
        })
        .then(() => {
          onSuccess()
        })
        .catch(e => {
          console.error(e)
          window.alert('Impossible de créer un utilisateur.')
        })
    }
  }

  _setAdmin = () => {
    const {
      credentials
    } = this.props.admin

    this.setState({
      email: {
        value: credentials.email,
        error: null
      }
    })
  }
}

export default SmartAdminForm
