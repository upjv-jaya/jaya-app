import React from 'react'
import PropTypes from 'prop-types'

import { Loading } from '../../components'
import { makeQuery } from '../../services/data-service'
import history from '../../routing/history'
import SmartAdminForm from './SmartAdminForm'

class SmartAdminsModify extends React.Component {
  static propTypes = {
    match: PropTypes.object
  }

  state = {
    loading: true,
    paramError: false,
    admin: null
  }

  componentDidMount () {
    const { id } = this.props.match.params
    if (isNaN(parseInt(id))) {
      history.push('/404')
    } else {
      this._getAdmin()
    }
  }

  render () {
    const { admin, loading } = this.state

    return (
      <div style={{ paddingTop: '2rem', paddingLeft: '1rem' }}>
        <h3 className='title is-3'>Modification d'un administrateur</h3>
        <section>
          { loading && <Loading /> }
          { admin != null && (
            <SmartAdminForm
              admin={admin}
              onSuccess={this._returnToList}
              onCancel={this._returnToList}
            />
          )}
        </section>
      </div>
    )
  }

  _getAdmin = () => {
    const { id } = this.props.match.params

    const query = `
      query {
        adminById(id: "${id}") {
          id
          credentials {
            id
            email
          }
        }
      }
    `

    makeQuery({ query, withAuthorization: true })
      .then(admin => this.setState({ loading: false, admin }))
      .catch(e => {
        console.error(e)
        window.alert('Impossible de récupérer l\'admin')
      })
  }

  _returnToList = () => history.push('/admin/admins')
}

export default SmartAdminsModify
