import React from 'react'
import SmartIdentityContent from '../../containers/common/SmartIdentityContent'
import SmartSideBar from '../../containers/common/SmartSideBar'
import SmartAdminsHome from '../../containers/admin/SmartAdminsHome'
import SmartAdminsAdd from '../../containers/admin/SmartAdminsAdd'
import SmartAdminsModify from '../../containers/admin/SmartAdminsModify'
import { Switch, Route } from 'react-router'

const AdminAdminsRouter = () => (
  <div className='adminColors'>
    <SmartSideBar />
    <div className='homeContainer'>
      <SmartIdentityContent />
      <Switch>
        <Route exact path='/admin/admins' component={SmartAdminsHome} />
        <Route exact path='/admin/admins/add' component={SmartAdminsAdd} />
        <Route exact path='/admin/admins/modify/:id' component={SmartAdminsModify} />
      </Switch>
    </div>
  </div>
)

export default AdminAdminsRouter
