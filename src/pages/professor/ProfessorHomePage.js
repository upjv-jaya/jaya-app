import React from 'react'
import SmartSideBar from '../../containers/common/SmartSideBar'
import SmartIdentityContent from '../../containers/common/SmartIdentityContent'
import SmartTeachingSummary from '../../containers/professor/SmartTeachingSummary'

function ProfessorHomePage () {
  return (
    <div>
      <SmartSideBar />
      <div className='homeContainer'>
        <SmartIdentityContent />
        <SmartTeachingSummary />
      </div>
    </div>
  )
}

export default ProfessorHomePage
