import React from 'react'
import SmartIdentityContent from '../../containers/common/SmartIdentityContent'
import SmartSideBar from '../../containers/common/SmartSideBar'
import { Switch, Route } from 'react-router'

import SmartTeachingFormAdd from '../../containers/professor/SmartTeachingFormAdd'
import SmartTeachingFormModify from '../../containers/professor/SmartTeachingFormModify'

const AdminSpecializationsRouter = () => (
  <div>
    <SmartSideBar />
    <div className='homeContainer'>
      <SmartIdentityContent />
      <Switch>
        <Route exact path='/professor/subject/add' component={SmartTeachingFormAdd} />
        <Route exact path='/professor/subject/modify/:id' component={SmartTeachingFormModify} />
      </Switch>
    </div>
  </div>
)

export default AdminSpecializationsRouter
