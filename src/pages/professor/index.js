export { default as ProfessorHomePage } from './ProfessorHomePage'
export { default as ProfessorConnectionPage } from './ProfessorConnectionPage'
export { default as ProfessorRegisterPage } from './ProfessorRegisterPage'
export { default as ProfessorTeachingRouter } from './ProfessorTeachingRouter'
