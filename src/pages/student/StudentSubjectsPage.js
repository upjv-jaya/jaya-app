import React from 'react'

import SmartIdentityContent from '../../containers/common/SmartIdentityContent'
import SmartSideBar from '../../containers/common/SmartSideBar'
import SmartSubjectsChoosen from '../../containers/student/SmartSubjectsChoosen'

const StudentSubjectsPage = (props) => (
  <div>
    <SmartSideBar />
    <div className='homeContainer'>
      <SmartIdentityContent />
      <SmartSubjectsChoosen />
    </div>
  </div>
)

export default StudentSubjectsPage
