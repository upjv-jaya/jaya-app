import React from 'react'

import SmartLogoutButton from '../../containers/common/SmartLogoutButton'

function AccountNotReadyPage () {
  return (
    <div className='disabledAccount_container'>
      <h1 className='title is-1'>Compte inactif</h1>
      <p className='disabledAccount_message'>Ce compte n'a pas encore été validé par l'administrateur de la plateforme.</p>
      <SmartLogoutButton />
    </div>
  )
}

export default AccountNotReadyPage
