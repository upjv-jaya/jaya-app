import React from 'react'
import PropTypes from 'prop-types'

import SubjectDropdown from '../common/SubjectDropdown'

function isDisabled (loading, error, subject) {
  return (
    subject.value == null ||
    subject.error != null ||
    loading ||
    error != null
  )
}

const TeachingForm = ({
  modify,
  subject,
  lecture,
  tutorial,
  error,
  loading,
  onChange,
  onCancel,
  onFormSubmit
}) => (
  <form onSubmit={onFormSubmit}>
    <div className='field'>
      <label className='label'>Nombre d'heure de cours</label>
      <p className='control'>
        <input
          className='input'
          type='number'
          name='lecture'
          placeholder='30...'
          onChange={onChange}
          value={lecture.value}
        />
      </p>
    </div>

    <div className='field'>
      <label className='label'>Nombre d'heure de TD</label>
      <p className='control'>
        <input
          className='input'
          type='number'
          name='tutorial'
          placeholder='30...'
          onChange={onChange}
          value={tutorial.value}
        />
      </p>
    </div>

    <SubjectDropdown
      onChange={item =>
        onChange({
          target: {
            name: 'subject',
            value: item
          }
        })
      }
      selectedItem={subject.value}
    />

    <div className='field'>
      <div className='control'>
        <button
          className={`button is-primary ${loading ? 'is-loading' : ''}`}
          disabled={
            isDisabled(loading, error, subject)
          }
          type='submit'
        >
          {modify ? 'Modifier' : 'Ajouter'}
        </button>
        <button
          className='button'
          disabled={loading}
          onClick={onCancel}
          type='button'
        >
          Annuler
        </button>
      </div>
    </div>
  </form>
)

TeachingForm.propTypes = {
  lecture: PropTypes.shape({
    value: PropTypes.number,
    error: PropTypes.string
  }),
  tutorial: PropTypes.shape({
    value: PropTypes.number,
    error: PropTypes.string
  }),
  subject: PropTypes.shape({
    value: PropTypes.shape({
      id: PropTypes.string,
      label: PropTypes.string
    }),
    error: PropTypes.string
  }),
  modify: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.string,
  onChange: PropTypes.func,
  onCancel: PropTypes.func,
  onFormSubmit: PropTypes.func
}

TeachingForm.defaultProps = {
  lecture: {
    value: 0,
    error: null
  },
  tutorial: {
    value: 0,
    error: null
  },
  subject: {
    value: null,
    error: null
  },
  modify: false,
  loading: false,
  error: null,
  onChange: () => {},
  onCancel: () => {},
  onFormSubmit: () => {}
}

export default TeachingForm
