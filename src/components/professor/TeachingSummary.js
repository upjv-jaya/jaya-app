import React from 'react'
import PropTypes from 'prop-types'

const TeachingSummary = ({
  subjects = [],
  onDeleteClick,
  onModifyClick
}) => (
  <table className='table'>
    <thead>
      <tr>
        <th>UE</th>
        <th>Heures de cours</th>
        <th>Heures de TD</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {
        subjects.map(ps => (
          <tr key={ps.specializationLabel}>
            <td>{ps.specializationLabel}</td>
            <td>{ps.lectureHours}</td>
            <td>{ps.tutorialHours}</td>
            <td />
          </tr>
        ))
      }
    </tbody>
  </table>
)

TeachingSummary.propTypes = {
  subjects: PropTypes.shape()
}

export default TeachingSummary
