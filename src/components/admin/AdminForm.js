import React from 'react'
import PropTypes from 'prop-types'

function isDisabled (modify, loading, error, email, password) {
  return loading || error != null ||
    email.value.length === 0 || email.error != null ||
    (!modify && (password.error != null || password.value.length === 0))
}

function AdminForm ({
  email,
  password,
  loading,
  modify,
  error,
  onChange,
  onFormSubmit,
  onCancel
}) {
  return (
    <form onSubmit={onFormSubmit}>
      <div className='field'>
        <label className='label' htmlFor='email'>Email</label>
        <div className='control'>
          <input
            className='input'
            type='email'
            name='email'
            value={email.value}
            onChange={onChange}
          />
        </div>
      </div>
      {!modify && (
        <div className='field'>
          <label className='label' htmlFor='password'>Mot de passe</label>
          <div className='control'>
            <input
              className='input'
              type='password'
              name='password'
              value={password.value}
              onChange={onChange}
            />
          </div>
        </div>
      )}
      <div className='field'>
        <div className='control'>
          <button
            className={`button is-primary ${loading ? 'is-loading' : ''}`}
            disabled={
              isDisabled(modify, loading, error, email, password)
            }
            type='submit'
          >
            {modify ? 'Modifier' : 'Ajouter'}
          </button>
          <button
            className='button'
            disabled={loading}
            onClick={onCancel}
            type='button'
          >
            Annuler
          </button>
        </div>
      </div>
    </form>
  )
}

AdminForm.propTypes = {
  email: PropTypes.shape({
    value: PropTypes.string,
    error: PropTypes.string
  }),
  password: PropTypes.shape({
    value: PropTypes.string,
    error: PropTypes.string
  }),
  loading: PropTypes.bool,
  modify: PropTypes.bool,
  error: PropTypes.string,
  onChange: PropTypes.func,
  onFormSubmit: PropTypes.func,
  onCancel: PropTypes.func
}

AdminForm.defaultProps = {
  firstName: {
    value: '',
    error: null
  },
  lastName: {
    value: '',
    error: null
  },
  email: {
    value: '',
    error: null
  },
  password: {
    value: '',
    error: null
  },
  loading: false,
  error: null,
  onChange: () => {},
  onFormSubmit: () => {},
  onCancel: () => {}
}

export default AdminForm
